package plus.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
/**
 * 启动类
 */
public class GHLPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(GHLPlusApplication.class, args);
    }
}